const Boleto = require('../../../index').Boleto
const moment = require('moment')
const expect = require('chai').expect

describe('Itau Boleto', () => {
  describe('when creating a valid boleto', () => {
    let boletos = []
    before(() => {
      boletos.push(new Boleto({
        'banco': 'itau',
        'data_emissao': moment('2017-01-01T00:00:00Z').valueOf(),
        'data_vencimento': moment('2017-01-05T00:00:00Z').valueOf(),
        'valor': 1500,
        'nosso_numero': '1',
        'numero_documento': '1',
        'cedente': 'Pagar.me Pagamentos S/A',
        'cedente_cnpj': '18727053000174',
        'agencia': '0057',
        'codigo_cedente': '12345',
        'carteira': '109',
        'pagador_nome': 'Nome do pagador',
        'pagador_documento':'0000000000',
        'pagador_logradouro_numero': 'Rua Henrique Dummont, 321',
        'pagador_cidade_estado_cep': 'Ribeirão Preto-SP-14085330',
        'local_de_pagamento': 'PAGÁVEL EM QUALQUER BANCO ATÉ O VENCIMENTO.',
        'instrucoes': 'Sr. Caixa, aceitar o pagamento e não cobrar juros após o vencimento.'
      }))
    })

    before(() => {
      boletos.push(new Boleto({
        'banco': 'itau',
        'data_emissao': moment('2017-01-01T00:00:00Z').valueOf(),
        'data_vencimento': moment('2017-01-05T00:00:00Z').valueOf(),
        'valor': 1500,
        'nosso_numero': '1',
        'numero_documento': '1',
        'cedente': 'Pagar.me Pagamentos S/A',
        'cedente_cnpj': '18727053000174',
        'agencia': '0057',
        'codigo_cedente': '12345',
        'carteira': '109',
        'pagador_nome': 'Nome do pagador',
        'pagador_documento':'0000000000',
        'pagador_logradouro_numero': 'Rua Henrique Dummont, 321',
        'pagador_cidade_estado_cep': 'Ribeirão Preto-SP-14085330',
        'local_de_pagamento': 'PAGÁVEL EM QUALQUER BANCO ATÉ O VENCIMENTO.',
        'instrucoes': 'Sr. Caixa, aceitar o pagamento e não cobrar juros após o vencimento.'
      }))
    })

    before(() => {
      boletos.push(new Boleto({
        'banco': 'itau',
        'data_emissao': moment('2017-01-01T00:00:00Z').valueOf(),
        'data_vencimento': moment('2017-01-05T00:00:00Z').valueOf(),
        'valor': 1500,
        'nosso_numero': '1',
        'numero_documento': '1',
        'cedente': 'Pagar.me Pagamentos S/A',
        'cedente_cnpj': '18727053000174',
        'agencia': '0057',
        'codigo_cedente': '12345',
        'carteira': '109',
        'pagador_nome': 'Nome do pagador',
        'pagador_documento':'0000000000',
        'pagador_logradouro_numero': 'Rua Henrique Dummont, 321',
        'pagador_cidade_estado_cep': 'Ribeirão Preto-SP-14085330',
        'local_de_pagamento': 'PAGÁVEL EM QUALQUER BANCO ATÉ O VENCIMENTO.',
        'instrucoes': 'Sr. Caixa, aceitar o pagamento e não cobrar juros após o vencimento.'
      }))
    })

    before(() => {
      boletos.push(new Boleto({
        'banco': 'itau',
        'data_emissao': moment('2017-01-01T00:00:00Z').valueOf(),
        'data_vencimento': moment('2017-01-05T00:00:00Z').valueOf(),
        'valor': 1500,
        'nosso_numero': '1',
        'numero_documento': '1',
        'cedente': 'Pagar.me Pagamentos S/A',
        'cedente_cnpj': '18727053000174',
        'agencia': '0057',
        'codigo_cedente': '12345',
        'carteira': '109',
        'pagador_nome': 'Nome do pagador',
        'pagador_documento':'0000000000',
        'pagador_logradouro_numero': 'Rua Henrique Dummont, 321',
        'pagador_cidade_estado_cep': 'Ribeirão Preto-SP-14085330',
        'local_de_pagamento': 'PAGÁVEL EM QUALQUER BANCO ATÉ O VENCIMENTO.',
        'instrucoes': 'Sr. Caixa, aceitar o pagamento e não cobrar juros após o vencimento.'
      }))
    })

    before(() => {
      boletos.push(new Boleto({
        'banco': 'itau',
        'data_emissao': moment('2017-01-01T00:00:00Z').valueOf(),
        'data_vencimento': moment('2017-01-05T00:00:00Z').valueOf(),
        'valor': 1500,
        'nosso_numero': '1',
        'numero_documento': '1',
        'cedente': 'Pagar.me Pagamentos S/A',
        'cedente_cnpj': '18727053000174',
        'agencia': '0057',
        'codigo_cedente': '12345',
        'carteira': '109',
        'pagador_nome': 'Nome do pagador',
        'pagador_documento':'0000000000',
        'pagador_logradouro_numero': 'Rua Henrique Dummont, 321',
        'pagador_cidade_estado_cep': 'Ribeirão Preto-SP-14085330',
        'local_de_pagamento': 'PAGÁVEL EM QUALQUER BANCO ATÉ O VENCIMENTO.',
        'instrucoes': 'Sr. Caixa, aceitar o pagamento e não cobrar juros após o vencimento.'
      }))
    })

    it('contains correct bank options', () => {
      boletos.forEach(boleto => {
        expect(boleto.bank.options).to.have.property('logoURL').that.contains('itau.jpeg')
        expect(boleto.bank.options).to.have.property('codigo', '341')
      })
    })

    it('contains correct codigo_banco', () => {
      boletos.forEach(boleto => {
        expect(boleto.codigo_banco).to.equal('341-7')
      })
    })

    it('contains correct barcode_data', () => {
      boletos.forEach(boleto => {
        expect(boleto.barcode_data).to.equal('34193703000000015001090000000160057123457000')
      })
    })

    it('contains correct linha_digitavel', () => {
      boletos.forEach(boleto => {
        expect(boleto.linha_digitavel).to.equal('34191.09008 00000.160051 71234.570001 3 70300000001500')
      })
    })
  })
})
